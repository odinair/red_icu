# red_icu

An extension on top of [Red's](https://github.com/Cog-Creators/Red-DiscordBot) built-in translator designed
to make use of the ICU message format for messages.

This isn't a 100% drop-in replacement; more information on this is in [Usage](#usage).

Install
--------

TODO

Usage
------

```python
from red_icu import Translator, cog_i18n
from redbot.core import commands

_ = Translator("MyCog", __file__)


@cog_i18n(_)
class MyCog(commands.Cog):
    """My super awesome cog"""

    async def examplecommand(self, ctx: commands.Context):
        """Example command"""
        # The .format() is important! This translator doesn't keep the same typings of
        # Red's built-in translator. Instead, now your translator will return a LazyStr
        # when called, instead of a typical str. While the returned LazyStr *attempts*
        # to act like a str - that's all it can do, *attempt* to look like a str.
        await ctx.send(_("Hello world").format())
```
