from __future__ import annotations

import logging
import os
from pathlib import Path
from typing import Any, Callable, Dict, Tuple, Union

from pyseeyou import ICUMessageFormat
from redbot.core import commands
from redbot.core.i18n import Translator as _Translator

# noinspection PyProtectedMember
from redbot.core.i18n import get_locale, get_regional_format

from red_icu.humanize import Humanize
from red_icu.util import ICUString, LazyStr

log = logging.getLogger("red.icu")
__all__ = ("Translator", "cog_i18n", "command_format_args")


def _builtin_transformer(_, kwargs: dict):
    for k, v in kwargs.copy().items():
        if isinstance(v, Humanize):
            kwargs[k] = v(locale=get_regional_format())
        elif isinstance(v, LazyStr):
            kwargs[k] = str(v)


def cog_i18n(translator: Translator):
    def decorator(cog_class: type):
        cog_class.__translator__ = translator
        for name, attr in cog_class.__dict__.items():
            if isinstance(attr, (commands.Group, commands.Command)):
                kwargs = getattr(attr.callback, "__icu_format_args__", {})
                attr.translator = translator.lazy(attr.callback.__doc__ or "", **kwargs)
                # Red doesn't copy the translator when commands are copied, and d.py copies commands upon
                # class creation. Why the translator isn't copied? /shrug
                attr.__original_kwargs__.setdefault("i18n", attr.translator)
                setattr(cog_class, name, attr)
        return cog_class

    return decorator


def command_format_args(**kwargs):
    def decorator(func):
        attach_to = func.callback if isinstance(func, commands.Command) else func
        attach_to.__icu_format_args__ = kwargs
        return func

    return decorator


class Translator(_Translator):
    def __init__(
        self, name: str, file_location: Union[str, Path, os.PathLike], default_locale: str = "en-US"
    ):
        super().__init__(name, file_location)
        self._default_locale = default_locale
        self._cache: Dict[Tuple[str, str], ICUString] = {}
        self.transformers = [_builtin_transformer]

    def transformer(self, func: Callable[[str, dict], Any]) -> Callable:
        """Attach a transformer to be called when translating strings

        Transformers modify translation arguments; transformers added last are called first.

        .. important::
            Modify the given keyword arguments dict in-place; the return value of called transformers
            are completely ignored.

        .. note:
            The given ``locale`` will be the bot's regional locale, and as such may not be
            exactly the same as the bot's locale.

        Example
        -------
        >>> _ = Translator(...)
        >>> @_.transformer
        ... def transformer(locale: str, kwargs: dict):
        ...     ...
        """
        self.transformers.insert(0, func)
        return func

    def __call__(self, untranslated: str) -> LazyStr:
        return self.lazy(untranslated)

    # This is separate from __call__ to be explicit in the fact that you shouldn't pass keyword arguments
    # when calling the translator.
    def lazy(self, untranslated: str, **kwargs) -> LazyStr:
        return LazyStr(self._translate_logic, [untranslated], kwargs)

    def _translate_logic(self, untranslated: str, **kwargs) -> str:
        locale = get_locale()
        if locale in self.translations and isinstance(self.translations[locale], dict):
            locale_data = self.translations[locale]
        else:
            locale_data = self.translations
        try:
            translated = locale_data[untranslated]
        except KeyError:
            translated = untranslated
            locale = self._default_locale
        cache_key = (locale.lower(), translated)
        if cache_key not in self._cache:
            self._cache[cache_key] = ICUString(self, locale, ICUMessageFormat.parse(translated))
        return self._cache[cache_key].format(**kwargs)
